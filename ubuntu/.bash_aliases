##
# bash aliases:
##
alias .b='source ~/.bashrc'
alias .ba='source ~/.bash_aliases'
alias .bf='source ~/.bash_functions'

alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ..; cd..'
alias ....='...; cd ..'
alias prevd='cd -'

alias mkdir='mkdir -pv'

alias top='htop'
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"

alias fhere='find . -iname '

alias du='ncdu'
alias df='pydf'
#alias df='df -hT | sort -r -k 6'
alias free='free -mt'
alias diff='colordiff'

alias ls='ls --color'
alias la='ls -Alph'
alias lar='ls -Alphr'
alias larl='ls -Alphr | less'
alias lal='ls -Alph | less'

alias wget='wget -c'

alias chrome='google-chrome'

alias histg='history | grep'

alias upd='sudo apt update'
alias upg='sudo apt upgrade'
alias aptinstall='sudo apt install $@'

alias status='sudo systemctl status $1'

alias readmail='cat /var/spool/mail/$USER'

alias gs='git status'
alias gpush='git push'
alias gpull='git pull'

alias bash2repo='copy_bashrc_files_to_repo'
