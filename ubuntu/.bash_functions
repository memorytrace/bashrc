##
# bash functions:
##

function lsgrep() {
    la $1 | grep $2
}
function mcd() {
    mkdir $1
    cd $1
    echo "PWD is now: $(echo $PWD)"
}
function tvim() {
    touch $1
    vim $1
}
function show_ip() {
    # ip addr show eth0 | grep inet | awk '{ print $2; }' | sed 's/\/.*$//'
    ip addr show wlp6s0 | grep inet | awk '{ print $2; }' | sed 's/\/.*$//'
}
# Useful for creating new .sh files with vim
function vimsh() { 
    if [[ -s $1 ]] ; then 
      echo -e "\nWarning: vimsh is exiting. File $1 has a size greater than zero; vimsh will not overwrite files.";
    elif [[ $1 == *.sh ]] ; then
      touch $1;
      chmod u+x $1;
      echo -n "#!" >> $1;
      echo "/bin/bash" >> $1;
      echo "# $1:  " >> $1;
      vim +:2 $1;
    else
      echo -e "\nOpening newfile $1 into vim...\n";
      sleep .5;
      vim $1;
    fi
}
# Useful for creating new .sh files with sublime
# TODO: support more than 1 argument
function subsh() {  
    case $# in
    0)
      echo "Usage: subsh [newfile.sh]"
      echo "Opening sublime without a file . . ."
      ;;
    esac

    if [[ -s $1 ]] ; then
      echo -e "\nWarning: subsh is exiting. File $1 has a size greather then zero; subsh will not overwrite files.";
    elif [[ $1 == *.sh ]] ; then
      touch $1
      chmod u+x $1
      echo -n "#!" > $1
      echo "/bin/bash" >> $1
      echo -n "# $1: " >> $1
      sub $1
    else 
      sub $1
    fi
}
function extract() {
    if [ -z "$1" ] ; then
    # if $1 has a zero value, show usage
    echo "Usage: extract <path/to.file>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    else
    if [ -f $1 ] ; then
    # We may need slicing to create a new directory:
    # NAME=${1%.*}
    # mkdir $NAME && cd $NAME
    case $1 in
      *.tar.bz2)   tar xvjf ./$1    ;;
      *.tar.gz)    tar xvzf ./$1    ;;
      *.tar.xz)    tar xvJf ./$1    ;;
      *.lzma)      unlzma ./$1      ;;
      *.bz2)       bunzip2 ./$1     ;;
      *.rar)       unrar x -ad ./$1 ;;
      *.gz)        gunzip ./$1      ;;
      *.tar)       tar xvf ./$1     ;;
      *.tbz2)      tar xvjf ./$1    ;;
      *.tgz)       tar xvzf ./$1    ;;
      *.zip)       unzip ./$1       ;;
      *.Z)         uncompress ./$1  ;;
      *.7z)        7z x ./$1        ;;
      *.xz)        unxz ./$1        ;;
      *.exe)       cabextract ./$1  ;;
      *)           echo "extract: '$1' - unknown archive method" ;;
    esac
    else
        echo "$1 - file does not exist"
    fi
fi
}
# Docker related
function centos() {
    docker exec -it centos_base /bin/bash
}
function ubuntu() {
    docker exec -it ubuntu /bin/bash
}
# .bashrc related. Read .bash_aliases if something in these looks unfamiliar
function vim-bashrc() {
    cp ~/.bashrc{,.bak} && vim ~/.bashrc && .b && bash2repo
}
function vim-profile() {
    cp ~/.profile{,.bak} && vim ~/.profile
}
function vim-bashaliases() { 
    cp ~/.bash_aliases{,.bak} && vim ~/.bash_aliases && .ba && bash2repo
}
function vim-bashfunctions() {
    cp ~/.bash_functions{,.bak} && vim ~/.bash_functions && .bf && bash2repo
}
function copy_bashrc_files_to_repo() {
    cp ~/.bashrc ~/Code/bash/bashrc/ubuntu/
    cp ~/.bash_aliases ~/Code/bash/bashrc/ubuntu/
    cp ~/.bash_functions ~/Code/bash/bashrc/ubuntu/
    sed -i '/^alias memtrace/d' ~/Code/bash/bashrc/ubuntu/.bash_aliases
}
# Get a look at a free daily IT ebook
function check-freebook() {
    w3m packtpub.com/packt/offers/free-learning | grep -A 8 "Time is running out to claim this free ebook" 
}
