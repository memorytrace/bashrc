# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

##
# Prompt:
##
function nonzero_return() {
    RETVAL=$?
    [ $RETVAL -ne 0 ] && echo " $RETVAL "
}

export PS1="\n╭─\[\e[02;36m\]\u\[\e[m\] at \[\e[02;32m\]\h\[\e[m\] in \[\e[02;95m\]\w\[\e[m\] \n╰─\[\e[01;31m\]\`nonzero_return\`\[\e[m\]{\[\e[02;39m\]\!\[\e[m\]} \[\e[34m\]λ\[\e[m\] "

# We need to source ~/.bash_aliases and ~/.bash_functions
if [ -f ~/.bash_aliases ]
    then
    . ~/.bash_aliases
fi

if [ -f ~/.bash_functions ]
    then
    . ~/.bash_functions
fi
